package ru.t1.vlvov.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.vlvov.tm.dto.model.AbstractUserOwnedModelDTO;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO> extends AbstractDtoService<M> implements IUserOwnedDtoService<M> {

}
