package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.service.dto.ISessionDtoService;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.enumerated.CustomSort;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.repository.dto.SessionDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO> implements ISessionDtoService {

    @Autowired
    @NotNull
    private SessionDtoRepository sessionRepositoryDTO;

    @Override
    @Transactional
    public void clear() {
        sessionRepositoryDTO.deleteAll();
    }

    @Override
    @Transactional
    public void add(@Nullable SessionDTO model) {
        if (model == null) throw new EntityNotFoundException();
        sessionRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable SessionDTO model) {
        if (model == null) throw new EntityNotFoundException();
        sessionRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<SessionDTO> collection) {
        clear();
        sessionRepositoryDTO.saveAll(collection);
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll() {
        return sessionRepositoryDTO.findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable SessionDTO model) {
        if (model == null) throw new EntityNotFoundException();
        sessionRepositoryDTO.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        sessionRepositoryDTO.deleteById(id);
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepositoryDTO.existsById(id);
    }

    @Override
    public @Nullable SessionDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepositoryDTO.findFirstById(id);
    }

    @Override
    @Transactional
    public void add(@Nullable String userId, @Nullable SessionDTO model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        model.setUserId(userId);
        sessionRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable String userId, @Nullable SessionDTO model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        model.setUserId(userId);
        sessionRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        sessionRepositoryDTO.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return sessionRepositoryDTO.findAllByUserId(userId);
    }

    @Override
    public @Nullable SessionDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return sessionRepositoryDTO.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable String userId, @Nullable SessionDTO model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        sessionRepositoryDTO.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        SessionDTO model = findOneById(userId, id);
        remove(userId, model);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll(@Nullable String userId, @Nullable CustomSort sort) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (sort == null) return findAll(userId);
        final Sort sortable = Sort.by(Sort.Direction.ASC, CustomSort.getOrderByField(sort));
        return sessionRepositoryDTO.findAllByUserId(userId, sortable);
    }

}
