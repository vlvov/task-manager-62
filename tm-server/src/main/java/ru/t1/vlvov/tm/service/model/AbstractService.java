package ru.t1.vlvov.tm.service.model;

import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.service.model.IService;
import ru.t1.vlvov.tm.model.AbstractModel;

@Service
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

}
