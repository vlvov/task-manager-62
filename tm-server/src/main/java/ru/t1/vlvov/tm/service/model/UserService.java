package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.model.IUserService;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.entity.UserNotFoundException;
import ru.t1.vlvov.tm.exception.field.*;
import ru.t1.vlvov.tm.exception.user.ExistsLoginException;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.repository.model.UserRepository;
import ru.t1.vlvov.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Autowired
    @NotNull
    private UserRepository userRepository;

    @Override
    @NotNull
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User model = new User();
        model.setLogin(login);
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        userRepository.save(model);
        return model;
    }

    @Override
    @Nullable
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User model = new User();
        model.setLogin(login);
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        model.setEmail(email);
        userRepository.save(model);
        return model;
    }

    @Override
    @Nullable
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User model = new User();
        model.setLogin(login);
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        model.setRole(role);
        userRepository.save(model);
        return model;
    }

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userRepository.findByEmail(email);
    }

    @Override
    @NotNull
    @Transactional
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User model = findByLogin(login);
        if (model == null) throw new UserNotFoundException();
        remove(model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User model = findOneById(id);
        if (model == null) throw new UserNotFoundException();
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        update(model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public User updateUser(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User model = findOneById(id);
        if (model == null) throw new UserNotFoundException();
        model.setFirstName(firstName);
        model.setLastName(lastName);
        model.setMiddleName(middleName);
        update(model);
        return model;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    @NotNull
    @Transactional
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new IdEmptyException();
        @Nullable final User model = findByLogin(login);
        if (model == null) throw new UserNotFoundException();
        model.setLocked(true);
        update(model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new IdEmptyException();
        @Nullable final User model = findByLogin(login);
        if (model == null) throw new UserNotFoundException();
        model.setLocked(false);
        update(model);
        return model;
    }

    @Override
    @Transactional
    public void clear() {
        userRepository.deleteAll();
    }

    @Override
    @Transactional
    public void add(@Nullable User model) {
        if (model == null) throw new EntityNotFoundException();
        userRepository.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable User model) {
        if (model == null) throw new EntityNotFoundException();
        userRepository.save(model);
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<User> collection) {
        clear();
        userRepository.saveAll(collection);
    }

    @Override
    @Nullable
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable User model) {
        if (model == null) throw new EntityNotFoundException();
        userRepository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.existsById(id);
    }

    @Override
    public @Nullable User findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.findFirstById(id);
    }

}
