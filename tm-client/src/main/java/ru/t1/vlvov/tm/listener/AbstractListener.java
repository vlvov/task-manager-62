package ru.t1.vlvov.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.listener.IListener;
import ru.t1.vlvov.tm.api.model.ICommand;
import ru.t1.vlvov.tm.api.service.IServiceLocator;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public abstract class AbstractListener implements IListener {

    @NotNull
    @Autowired
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract Role[] getRoles();

    public abstract void handler(@NotNull final ConsoleEvent event);

    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String description = getDescription();
        @Nullable final String argument = getArgument();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    protected void setToken(@Nullable final String token) {
        serviceLocator.getTokenService().setToken(token);
    }

    @Nullable
    protected String getToken() {
        return serviceLocator.getTokenService().getToken();
    }

}
