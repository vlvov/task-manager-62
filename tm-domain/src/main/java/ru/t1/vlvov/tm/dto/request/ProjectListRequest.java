package ru.t1.vlvov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.CustomSort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private CustomSort sort;

    public ProjectListRequest(@Nullable final CustomSort sort) {
        this.sort = sort;
    }

    public ProjectListRequest(@Nullable String token) {
        super(token);
    }

}
