package ru.t1.vlvov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(@Nullable final String command) {
        super("Error! Command " + command + " not supported...");
    }

}
